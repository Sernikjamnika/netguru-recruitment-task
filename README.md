# Netguru-recruitment-task

## Installation

This is Netguru recruitment task - a basic movie database interacting with external API (omdbapi.com).

To run this app you have to have Docker installed. I assume you have already done it. If not firstly got to this [page](https://docs.docker.com/install/).

Then go and change [local variables](./local-var.env) and write down yor `API-KEY` to OMDB Api. If you don't have one go to www.omdbapi.com and generate one.

When Docker is installed go to project root directory and type this to build the image.
```sh
$ docker-compose build
```

Next type this to run the image
```sh
$ docker-compose up
```

Migrations will be applied automatically.

Now the api is avaliable [here](http://localhost:8000). Easy isn't it :-)

During building proccess those modules are downloaded:
- Django==2.2.3
- djangorestframework==3.9.4
- psycopg2-binary==2.8.3
- django-filter==2.1.0
- requests==2.22.0
- drf-yasg==1.16.0

## Usage

When server is running u can send requests to:
```
/omdb_api/movie
/omdb_api/comments
/omdb_api/top
```
For detailed info go to [redoc](http://localhost:8000/redoc/)

## Tests
To run tests for the project use this command. It will run all tests in the project.
```sh
$ docker exec -ti netguru-recruitment-task_backend_1 python manage.py test
```
