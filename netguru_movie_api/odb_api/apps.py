from django.apps import AppConfig


class OdbApiConfig(AppConfig):
    name = 'odb_api'
