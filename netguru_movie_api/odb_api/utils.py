import os
import requests

from .consts import OMDB_MOVIE_SEARCH_TEMPLATE
from .exceptions import OMDBApiConnectionError


def get_odb_movie_data(title):
    """
    Fetches data about movie with `title` from
    OMDB Api using `requests` module.

    It requires OMDB api key to be ing
    environmental variables in  `API_KEY`
    """

    url_template = OMDB_MOVIE_SEARCH_TEMPLATE
    api_key = os.environ.get('API_KEY')
    try:
        response = requests.get(url_template.format(title=title, api_key=api_key))
    except requests.ConnectionError:
        raise OMDBApiConnectionError
    return response.json()
