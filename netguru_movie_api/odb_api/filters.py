from django_filters import rest_framework as filters

from .models import Movie


class MovieFilter(filters.FilterSet):
    """
    Enables filtering movies by their title with lookup
    expresion `startswith`
    """

    data__Title = filters.CharFilter(field_name='data__Title', lookup_expr='startswith', label='Title')

    class Meta:
        model = Movie
        fields = ['data__Title',]
