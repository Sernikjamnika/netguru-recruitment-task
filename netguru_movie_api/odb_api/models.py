from django.db import models
from django.contrib.postgres.fields import JSONField


class Movie(models.Model):
    """
    Stores `id` of movie and `data` as JSON
    """

    data = JSONField()

    def __str__(self):
        return self.data.get("Title", "No title")

class Comment(models.Model):
    """
    Stores `body`, `creation date` of the comment
    and `related movie`

    `Comment` is deleted automaticall while related Movie
    is erased

    **Note** Creation date is not editable and is
    set while `Comment` creation
    """

    body = models.TextField()
    movie = models.ForeignKey(Movie, on_delete=models.CASCADE)
    create_date = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.body
