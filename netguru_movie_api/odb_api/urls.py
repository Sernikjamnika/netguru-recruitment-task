from django.urls import path

from .views import CommentsView, MovieRankingView, MovieView


urlpatterns = [
    path('comments/', CommentsView.as_view()),
    path('movies/', MovieView.as_view()),
    path('top/', MovieRankingView.as_view()),
]
