from rest_framework import serializers

from .exceptions import MovieNotExistInOMDBApi
from .models import Comment, Movie
from .utils import get_odb_movie_data


class MovieSerializer(serializers.ModelSerializer):
    """
    Serializes `Movie` model

    While creation it `title` to fetch data from OMDB Api

    While retrieval serializes all fields

    Raises `MovieNotExistInOMDBApi` if movie title is not present
    in OMDB
    """

    title = serializers.CharField(write_only=True)

    def create(self, validated_data):
        title = validated_data.pop('title')
        movie_data = get_odb_movie_data(title)
        self.validate_movie_data(movie_data)
        validated_data['data'] = movie_data
        movie_obj, _ = Movie.objects.get_or_create(**validated_data)
        return movie_obj

    def validate_movie_data(self, movie_data):
        if movie_data.pop('Response', 'False') == 'False':
            raise MovieNotExistInOMDBApi

    class Meta:
        model = Movie
        fields = '__all__'
        read_only_fields = ('data',)


class CommentSerializer(serializers.ModelSerializer):
    """
    Serializes all `Comment` model fields
    """

    class Meta:
        model = Comment
        fields = '__all__'


class MovieRankingSerializer(serializers.Serializer):
    """
    Serializes ranking of movies

    Contains `movie_id`, `rank` and `total_comments'
    """

    movie_id = serializers.IntegerField(source='id')
    rank = serializers.IntegerField()
    total_comments = serializers.IntegerField()


class DateRangeSerializer(serializers.Serializer):
    """
    Serializes date range - `before_date` and `after_date`
    """

    before_date = serializers.DateField()
    after_date = serializers.DateField()
