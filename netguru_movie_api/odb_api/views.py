from django.db.models import Count, F, Q
from django.db.models.expressions import Window
from django.db.models.functions import Rank
from django.shortcuts import render
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters, generics, status
from rest_framework.response import Response

from .filters import MovieFilter
from .models import Comment, Movie
from .serializers import (CommentSerializer, DateRangeSerializer,
                          MovieRankingSerializer, MovieSerializer)


class MovieView(generics.ListCreateAPIView):
    """
    Returns, if in database, or creates movie based on given
    `title` whule POST

    Returns list of all movies in databse while GET

    Enables ordering by `id`, `data__Title`, `data__imdbRating`

    Enables filtering by `MovieFilter` class
    """

    serializer_class = MovieSerializer
    queryset = Movie.objects.all()
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter,)
    ordering_fields = ('id', 'data__Title', 'data__imdbRating',)
    filterset_class = MovieFilter

    def create(self, request, *args, **kwargs):
        try:
            movie = Movie.objects.get(data__Title=request.data.get('title', None))
            serializer = self.get_serializer(movie)
            headers = self.get_success_headers(serializer.data)
            return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
        except Movie.DoesNotExist:
            return super().create(request, *args, **kwargs)


class CommentsView(generics.ListCreateAPIView):
    """
    Creates comment and returns its JSON representation while POST

    Returns list of all comment while GET

    Enables filtering by `movie__id`

    """

    serializer_class = CommentSerializer
    queryset = Comment.objects.all()
    filter_backends = (DjangoFilterBackend,)
    filterset_fields = ('movie__id',)


class MovieRankingView(generics.ListAPIView):
    """
    Returns ranking of movies based on number of comments

    Requires parmeters such as `after_date` and `before_date`
    to get comments from given date range
    """

    serializer_class = MovieRankingSerializer

    def get_queryset(self):
        rank_by_total_comments = Window(
            expression=Rank(),
            order_by=F('total_comments').desc())

        params_serializer = DateRangeSerializer(data=self.request.query_params)
        params_serializer.is_valid(raise_exception=True)

        after_date = params_serializer.data['after_date']
        before_date = params_serializer.data['before_date']
        count_comments_in_date_range = Count('comment',
            filter=Q(comment__create_date__range=(after_date, before_date)))

        query = Movie.objects.annotate(
            total_comments=count_comments_in_date_range,
            rank=rank_by_total_comments
            ).values('id', 'total_comments', 'rank')
        return query
