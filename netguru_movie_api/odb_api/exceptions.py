from rest_framework import exceptions


class MovieNotExistInOMDBApi(exceptions.APIException):
    """
    Raises when moive's title is not present in OMDB database
    """

    status_code = 400
    default_detail = "Movie does not exist in OMDB Api"
    default_code = "bad_request"


class OMDBApiConnectionError(exceptions.APIException):
    """
    Raises when odb_api is not able to connect to OMDB Api
    """

    status_code = 503
    default_detail = "OMDBApi service temporarily unavailable, try again later"
    default_code = 'service_unavailable'
