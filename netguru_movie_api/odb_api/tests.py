from django.test import TestCase, TransactionTestCase
import requests

import datetime
from unittest import mock

from .models import Movie, Comment


MOVIES = [
    {
        'id': 1,
        'data': {
            'Title': 'Scream'
        }
    },
    {   
        'id': 2,
        'data': {
            'Title': 'Scream'
        }
    }
]

COMMENTS = [
    {
        "id": 1,
        "create_date": datetime.datetime.today().strftime("%Y-%m-%d"),
        "body": "Great movie",
        "movie": 1
    },
    {
        "id": 2,
        "create_date": datetime.datetime.today().strftime("%Y-%m-%d"),
        "body": "Not worth",
        "movie": 2
    }
]

def create_movies():
    for movie in MOVIES:
        Movie.objects.create(data=movie['data'])

def create_comments():
    for comment in COMMENTS:
        Comment.objects.create(
            body=comment['body'],
            movie=Movie.objects.get(pk=comment['movie']))


class MovieTest(TransactionTestCase):
    reset_sequences = True
    movie_url = '/omdb_api/movies/'

    def setUp(self):
        create_movies()
    
    def test_getting_all_movies(self):
        response = self.client.get(self.movie_url)

        self.assertEqual(response.json(), MOVIES)
        self.assertEqual(response.status_code, 200)
    
    @mock.patch('odb_api.utils.requests.get')
    def test_movie_creation(self, mock_get):
        mock_response = mock.Mock()
        expected_dict = {
            'Title': 'Titanic',
            'Response': 'True'
        }

        mock_response.json.return_value = expected_dict
        mock_response.status_code = 200

        mock_get.return_value = mock_response

        response_before = self.client.post(self.movie_url, {'title': 'Titanic'})
        movie_data_before = response_before.json()
        
        response_after = self.client.post(self.movie_url, {'title': 'Titanic'})
        movie_data_after = response_after.json()

        # check if the same data is returned
        self.assertEqual(movie_data_after, movie_data_before)
        # assert that external api was called once
        mock_get.assert_called_once()

    @mock.patch('odb_api.utils.requests.get')
    def test_external_api_fails_to_find_movie(self, mock_get):
        mock_response = mock.Mock()
        expected_dict = {
            "Response":"False",
            "Error":"Movie not found!"
        }
        mock_response.json.return_value = expected_dict
        mock_response.status_code = 404
        mock_get.return_value = mock_response
        response = self.client.post(self.movie_url, {'title': 'Nonexisting movie'})
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json(), {
            "detail": "Movie does not exist in OMDB Api"
        })

    @mock.patch('odb_api.utils.requests.get')
    def test_external_api_does_not_respond(self, mock_get):
        mock_get.side_effect = requests.ConnectionError('Test')
        response = self.client.post(self.movie_url, {'title': 'Something'})
        self.assertEqual(response.status_code, 503)


class CommentTest(TransactionTestCase):
    reset_sequences = True
    comments_url = '/omdb_api/comments/'

    def setUp(self):
        create_movies()
        create_comments()
    
    def test_getting_all_movies(self):
        response = self.client.get(self.comments_url)
        self.assertEqual(response.json(), COMMENTS)
        self.assertEqual(response.status_code, 200)

    def test_filtering_by_id(self):
        response = self.client.get(self.comments_url+'?movie__id=1')
        self.assertEqual(response.json(), [COMMENTS[0]])


class RankingTest(TransactionTestCase):
    reset_sequences = True
    date = datetime.datetime.today().strftime("%Y-%m-%d")
    ranking_url = f'/omdb_api/top/?before_date={date}&after_date={date}'

    def setUp(self):
        create_movies()
        create_comments()

    def test_ranking_changes_after_comment_addition(self):
        response_before = self.client.get(self.ranking_url)
        movie = Movie.objects.all()[0]
        Comment.objects.create(
            body="It was good",
            movie=movie
        )
        response_after = self.client.get(self.ranking_url)
        self.assertNotEqual(response_after.json(), response_before.json())
        total_comments_before = self.get_total_comments(response_before.json(), movie)
        total_comments_after = self.get_total_comments(response_after.json(), movie)
        self.assertEqual(total_comments_after, total_comments_before + 1)

    def test_ranking_is_sorted(self):
        movie = Movie.objects.all()[0]
        Comment.objects.create(
            body="It was good",
            movie=movie
        )
        response = self.client.get(self.ranking_url)
        ranking = response.json()
        for index in range(len(ranking) - 1):
            self.assertGreaterEqual(
                ranking[index]['total_comments'],
                ranking[index - 1]['total_comments']
            )

    def get_total_comments(self, ranking, movie):
        total_comments = 0
        for place in ranking:
            if place.get('movie_id', -1) == movie.id:
                total_comments = place.get('total_comments', 0)
        return total_comments
